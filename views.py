from flask import Blueprint, render_template, request, flash, redirect, jsonify
from flask.views import MethodView
from flask.ext import login, wtf
from models import User, Role, Design, Plan, Vote
from bson.objectid import ObjectId
import json, string, random
import stripe
from serve import bcrypt
from apikeys import *

#MAIN
main = Blueprint('main', __name__, template_folder="templates")

class MainView(MethodView):
	def get(self):
		special = ''
		if 'special' in request.args:
			special = request.args['special']
		from itsdangerous import TimestampSigner
		s = TimestampSigner('playpianolikewoah@gmail.com')
		string = s.sign('change-email')
		print string
		s.unsign(string)
		recent_designs = Design.objects(social=True).order_by('modified_at').limit(25)
		return render_template('main/home.html', recent_designs=recent_designs,special=special)

class AppView(MethodView):
	def get(self,file_id=None):
		no_cache = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(30))

		if not file_id:
			return render_template('main/app.html',user=login.current_user, no_cache=no_cache)
		else:
			return render_template('main/app.html', no_cache=no_cache, file_id=file_id)

class AboutView(MethodView):
	def get(self):
		return render_template('main/about.html')

class PlansView(MethodView):
	def get(self,options = None):
		if login.current_user.is_anonymous():
			link = '/register/'
		else:
			link = '/upgrade/'
		return render_template('main/plans.html',link=link)

class ResourcesView(MethodView):
	def get(self):
		return render_template('main/resources.html')

class RoadMapView(MethodView):
	def get(self):
		return render_template('main/roadmap.html')

class FeaturesView(MethodView):
	def get(self):
		return render_template('main/features.html')

main.add_url_rule('/', view_func=MainView.as_view('home'))
main.add_url_rule('/app/', view_func=AppView.as_view('app'))
main.add_url_rule('/app/<file_id>/', view_func=AppView.as_view('app'))
main.add_url_rule('/about/', view_func=AboutView.as_view('about'))
main.add_url_rule('/plans/', view_func=PlansView.as_view('plans'))
main.add_url_rule('/plans/<options>/', view_func=PlansView.as_view('plans'))
main.add_url_rule('/features/', view_func=FeaturesView.as_view('features'))
main.add_url_rule('/resources/', view_func=ResourcesView.as_view('resources'))
main.add_url_rule('/roadmap/', view_func=RoadMapView.as_view('roadmap'))

#USERS
users = Blueprint('users', __name__, template_folder="templates")

class LogoutView(MethodView):
	methods = ['GET', 'POST']

	@login.login_required
	def dispatch_request(self):
		login.logout_user()
		return redirect('/')

class LoginView(MethodView):
	methods = ['GET', 'POST']
	def dispatch_request(self):
		if login.current_user.is_anonymous():
			form = LoginForm(request.form)
			if form.validate_on_submit():
				user = form.get_user()
				if not login.login_user(user):
					flash('You are not an active user yet')
				return redirect('/designs/')
			return render_template('users/login.html', form=form)
		else:
			return redirect('/designs/')

class UserRegisterView(MethodView):
	def get(self,plan=None):
		
		if plan: 
			set_plan = Plan.objects(name=plan).first()
		else:
			set_plan = Plan.objects(name='free').first()

		if not set_plan: set_plan = Plan.objects(name='free').first()
		form = UserRegistrationForm(request.form)
		return render_template('users/register.html', form=form,plan=set_plan, pk=stripe_api_pushable)

	def post(self):
		plan = request.form['plan']
		form = UserRegistrationForm(request.form)
		if form.validate():
			user = User(email=form.email.data, 
				password=bcrypt.generate_password_hash(form.password.data),
				roles=Role.objects(name='user').first(),
				active=True)

			if plan != 'free':
				stripe.api_key = stripe_api_secret
				token = request.form['stripeToken']
				try:
					customer = stripe.Customer.create(card=token, plan=plan, email=user.email)
				except Exception, e:
					flash(str(e))
					return render_template('users/register.html', form=form, pk=stripe_api_pushable)
				user.stripe_id = customer.id
				user.plan = Plan.objects(name=plan).first()
			else:
				user.plan=Plan.objects(name='free').first()
			user.save()


			login.login_user(user)
			flash('Hey %s, you are ready to go!' % user.email)
			return redirect('/')
		return render_template('users/register.html', form=form,plan=plan.lower(), pk=stripe_api_pushable)

class LoginForm(wtf.Form):
	email = wtf.TextField(validators=[wtf.validators.Required()])
	password = wtf.PasswordField(validators=[wtf.validators.Required()])

	def validate_email(self,field):
		user = self.get_user()
		if user is None:
			raise wtf.ValidationError('Invalid email')

		if not bcrypt.check_password_hash(user.password,self.password.data):
			raise wtf.ValidationError('Invalid password')

	def get_user(self):
		user = User.objects(email=self.email.data).first()
		return user

class LoginCheck(MethodView):
	def post(self):
		if login.current_user.is_anonymous():
			return json.dumps({'email':None,'anon':login.current_user.is_anonymous()})
		else:
			return json.dumps({'email':login.current_user.email,'anon':login.current_user.is_anonymous()})

class UserRegistrationForm(wtf.Form):
    email = wtf.TextField(validators=[wtf.validators.Length(min=6, max=120),
    	wtf.validators.Email(message='Emails must be valid')])
    password = wtf.PasswordField(validators=[
		wtf.validators.Required(),
		wtf.validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = wtf.PasswordField('Repeat Password')
    plan = wtf.SelectField('plan', coerce=str, choices=[(p.name, p.name.capitalize()) for p in Plan.objects])
    accept_tos = wtf.BooleanField('I accept the TOS', [wtf.validators.Required()])

    def validate_email(self,field):
		used_email = User.objects(email=self.email.data).count()
		if used_email > 0:
			raise wtf.ValidationError('%s is already taken' % self.email.data)

class PlanForm(wtf.Form):
	name = wtf.SelectField('Plan', coerce=str, choices=[(p.name, p.name.capitalize()) for p in Plan.objects])

class UserSettingsView(MethodView):
	def get(self,chosen_plan=None):
		if login.current_user.is_anonymous():
			return redirect('/')
		else:
			user = User.objects(id=login.current_user.id).first()
			stripe.api_key = stripe_api_secret
			if not chosen_plan:
				plan = user.plan
			else:
				plan = Plan.objects(name=chosen_plan).first()
			form = PlanForm(request.form, obj=plan)
			return render_template('users/settings.html',user=user,chosen_plan=chosen_plan,plan=user.plan,form=form, pk=stripe_api_pushable)
	def post(self):
		plan = request.form['name']
		stripe.api_key = stripe_api_secret
		#credit card submitted by form
		user = User.objects(id=login.current_user.id).first()
		if not user.stripe_id:
			if plan != 'free':
				token = request.form['stripeToken']
				try:
					customer = stripe.Customer.create(card=token, plan=plan, email=login.current_user)
				except Exception, e:
					flash(str(e))
					return redirect('/settings/')
				user.stripe_id = customer.id
		else:
			try:
				customer = stripe.Customer.retrieve(user.stripe_id)
				customer.update_subscription(plan=plan, prorate="True")
			except Exception,e:
				flash(e)
				return redirect('/settings/')
		
		user_plan = Plan.objects(name=plan).first()
		user.plan = user_plan
		user.save()
		flash('Congrats! Your plan has been updated to the %s plan! Now go make some great designs!' % plan)
		return redirect('/?special=new_member')

class TermOfServiceView(MethodView):
	def get(self):
		print stripe_api_pushable
		return render_template('main/tos.html')

class WebhookEvent(MethodView):
	def post(self):
		import stripe
		import requests

		stripe.api_key = stripe_api_secret
		webhook_id = request.json['id']
		print 'got webhook'
		print webhook_id
		try:
			event = stripe.Event.retrieve(webhook_id)
			if event.type == 'invoiceitem.created':
				try:
					invoice = stripe.Invoice.retrieve(event['data']['id'])
					print invoice
					email_details = {
					'key': mandrill_api,
						'message':{
							'html': "<p>Example HTML content</p>",
		        			"text": "Example text content",
		        			"subject": "example subject",
		        			"from_email": "bisonkick@gmail.com",
		        			"from_name": "bisonkick",
		        			"to": [
		            			{
		                			"email": "playpianolikewoah@gmail.com",
		                			"name": "Jacob Schatz"
		            			}
		        			],
		        			"headers": {
		            			"Reply-To": "bisonkick@gmail.com"
		      			 	},
						}
					}

				except Exception, e:
					print e
				
				headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
				r = requests.post(mandrill_url,data=json.dumps(email_details),headers=headers)

		except Exception, e:
			print e
		
		return 'ok'

users.add_url_rule('/login/', view_func=LoginView.as_view('login'))
users.add_url_rule('/logout/', view_func=LogoutView.as_view('logout'))
users.add_url_rule('/register/',view_func=UserRegisterView.as_view('register'))
users.add_url_rule('/register/<plan>/',view_func=UserRegisterView.as_view('register'))
users.add_url_rule('/service/user/', view_func=LoginCheck.as_view('serviceuser'))
users.add_url_rule('/settings/',view_func=UserSettingsView.as_view('settings'))
users.add_url_rule('/upgrade/',view_func=UserSettingsView.as_view('upgrade'))
users.add_url_rule('/upgrade/<chosen_plan>/',view_func=UserSettingsView.as_view('upgrade'))
users.add_url_rule('/tos/',view_func=TermOfServiceView.as_view('tos'))
users.add_url_rule('/service/webhook/', view_func=WebhookEvent.as_view('subscriptionchange'))

designs = Blueprint('designs', __name__, template_folder="templates")

class DesignsSave(MethodView):
	def post(self):
		if login.current_user.is_anonymous():
			return jsonify({'status':'failure','id':0})
		save_data = request.form['save_data']
		save_title = request.form['title']
		screenshot = request.form['screenshot']
		save_id = request.form['id']
		save_title = save_title or 'untitled design'
		user = User.objects(id=login.current_user.id).first()
		d = Design(name=save_title,data=save_data,user=user,ss=screenshot)
		d.social = user.plan.name == 'free'

		if save_id:
			d.id = save_id;
		d.save()
		return jsonify({'status':'success','id':str(d.id)})

class DesignsLoad(MethodView):
	def post(self):
		load_id = request.form['load_id']
		d = Design.objects(id=load_id).first()
		if d:
			if login.current_user.is_anonymous():
				if d.social:
					return jsonify({'status':'success',
						'data':d.data,
						'name':d.name,
						'id':str(d.id),
						'social':d.social,
						'owner':False})
				else:
					return jsonify({'status':'failure'})
			else: 
				user = User.objects(id=login.current_user.id).first()
				owner = d.user == user
				if d.social:
					return jsonify({'status':'success',
						'data':d.data,
						'name':d.name,
						'id':str(d.id),
						'social':d.social,
						'owner':owner})
				elif d.user == user:
					return jsonify({'status':'success',
						'data':d.data,
						'name':d.name,
						'id':str(d.id),
						'social':d.social,
						'owner':True})	
				else:
					return jsonify({'status':'failure'})
		else:
			return jsonify({'status':'failure'})
class DesignsView(MethodView):
	def get(self):
		if not login.current_user.is_anonymous():
			user = User.objects(id=login.current_user.id).first()
			designs = Design.objects(user=user)
			return render_template('designs/designs.html', designs=designs)
		else:
			return redirect('/')

class DesignsSetSocial(MethodView):
	def post(self):
		design = request.form['design']
		social = request.form['social'] == 'true'
		d = Design.objects(id=ObjectId(design)).first()
		if social:
			d.social = social;
			d.save()
			return jsonify({ 'status':'success' })
		else:
			user = User.objects(id=login.current_user.id).first()
			used_private = Design.objects(user=user,social=social).count()
			if used_private < user.plan.limit:
				d.social = social;
				d.save()
				return jsonify({ 'status':'success' })
			else:
				return jsonify({ 'status':'failure' })

class DesignsDelete(MethodView):
	def post(self,design_id):
		user = User.objects(id=login.current_user.id).first()
		design = Design.objects(id=str(design_id),user=user).first()
		design.delete()
		design.save()
		return jsonify({ 'status':'success' })

class VoteService(MethodView):
	def post(self):
		if login.current_user.is_anonymous():
			return jsonify({'status':'failure'})
		design_id = request.form['design']
		up = request.form['up'] == 'true'
		print up
		user = User.objects(id=login.current_user.id).first()
		design = ObjectId(design_id)
		voted = bool(Vote.objects(user=user.id,design=design).count())
		if not voted:
			v = Vote(user=user.id,design=design)
			v.save()
			d = Design.objects(id=design).first()
			if up:
				d.rating += 1
			else:
				d.rating -= 1
			d.save()
			return jsonify({ 'status':'success','rating':d.rating })
		else:
			return jsonify({ 'status':'failure'})

users.add_url_rule('/designs/',view_func=DesignsView.as_view('designs'))
users.add_url_rule('/designs/delete/<design_id>/',view_func=DesignsDelete.as_view('designdelete'))
users.add_url_rule('/service/designs/save/',view_func=DesignsSave.as_view('designssave'))
users.add_url_rule('/service/designs/load/',view_func=DesignsLoad.as_view('designsload'))
users.add_url_rule('/service/designs/social/',view_func=DesignsSetSocial.as_view('designssetsocial'))
users.add_url_rule('/service/vote/',view_func=VoteService.as_view('voteservice'))



