from flask import Flask, render_template
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager, current_user
from flask.ext.bcrypt import Bcrypt

app = Flask(__name__)
app.debug = True
bcrypt = Bcrypt(app)
app.config["MONGODB_SETTINGS"] = {'DB' : 'bisonkick'}
app.config["SECRET_KEY"] = "bobbyhaddababyitzaboy"

db = MongoEngine(app)

def register_blueprints():
	from views import main
	from views import users
	app.register_blueprint(main)
	app.register_blueprint(users)

def init_login():
	from models import User
	from flask import g
	login_manager = LoginManager()
	login_manager.setup_app(app)
	login_manager.login_view = "/login/"
	def load_user(user_id):
		return User.objects(id=user_id).first()

	@app.before_request
	def before_request():
		g.user = current_user
	
	login_manager.user_loader(load_user)

def init_error_codes():
	@app.errorhandler(404)
	def page_not_found(e):
		return render_template('main/404.html')

register_blueprints()
init_login()
init_error_codes()

if __name__ == '__main__':
	app.run()