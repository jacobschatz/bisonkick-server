from serve import db
from flask.ext.login import UserMixin

class Role(db.Document):
	name = db.StringField(required=True,unique=True)
	
	def __unicode__(self):
		return self.name

class Plan(db.Document):
	name = db.StringField(required=True, unique=True)
	price = db.StringField(required=True)
	limit = db.IntField(default=0)

	def __unicode__(self):
		return self.name

class User(UserMixin,db.Document):
	email = db.StringField(max_length=120, required=True)
	password = db.StringField(max_length=64, required=True)
	role = db.ReferenceField(Role,dbref=True)
	active = db.BooleanField()
	stripe_id = db.StringField()
	plan = db.ReferenceField(Plan,dbref=True)

	def is_active(self):
		return self.active

	def is_authenticated(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return self.id

	def __unicode__(self):
		return self.email

class Design(db.Document):
	import datetime
	name = db.StringField(required=True)
	data = db.StringField()
	user = db.ReferenceField(User,dbref=True)
	social = db.BooleanField()
	ss = db.StringField()
	rating = db.IntField(default=0)
	modified_at = db.DateTimeField(default=datetime.datetime.now)

class Vote(db.Document):
	user = db.ObjectIdField()
	design = db.ObjectIdField()